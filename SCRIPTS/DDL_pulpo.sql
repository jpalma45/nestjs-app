CREATE TABLE tbl_vehicles(
    id serial primary key,
    carPlate varchar(25) not null unique,
    mark varchar(100) not null,
    model INT not null,
    colour varchar(50) not null,
    admissionDate Date not null,
    status varchar(25) not null,
    assigned varchar(100) not null
);

CREATE TABLE tbl_news(
    id serial primary key,
    affair varchar(250) not null unique,
    description varchar(250) not null unique,
    type varchar(100) not null unique,
    idVehicle INT not null,
    CONSTRAINT fk_vehicles
      FOREIGN KEY(idVehicle) 
	  REFERENCES tbl_vehicles(id)
)