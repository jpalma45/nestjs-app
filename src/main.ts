import { NestFactory } from '@nestjs/core';
import {
	BadRequestException,
	ValidationError,
	ValidationPipe,
} from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './application/app.module';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	app.enableCors();
	app.useGlobalPipes(
		new ValidationPipe({
			exceptionFactory: (validationErrors: ValidationError[] = []) => {
				return new BadRequestException(validationErrors);
			},
			disableErrorMessages: true,
			transform: true,
		}),
	);

	const options = new DocumentBuilder()
		.setTitle('test')
		.setDescription('This API permit manage information about the Pulpo App')
		.setVersion('1.0')
		.addTag('News', "News's resources")
		.addTag('Vehicles', "Vehicles's resources")
		.build();

	SwaggerModule.setup('/docs', app, SwaggerModule.createDocument(app, options));

	await app.listen(process.env.PORT || 8080);
}
bootstrap();
