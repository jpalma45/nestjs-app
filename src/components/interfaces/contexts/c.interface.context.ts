/**
 * @summary This interface allows to define context strategy
 */
export interface Context<I, O> {
	/**
	 * @summary This method allows to init the context on the aplication
	 * @param data Initial data.
	 */
	init(data?: I, callback?: any): O;

	/**
	 * @summary This method allows to get data information associated with the context
	 */
	getData(): O;

	/**
	 * @summary This method subscribes data information into context
	 * @param data Data information
	 * @callback Function
	 */
	registerData(data?: I, callback?: any): O;
}
