import { AsyncLocalStorage } from 'async_hooks';
import { Context } from './c.interface.context';

const context = new AsyncLocalStorage();

export class AsyncHookContext implements Context<any, any> {
	public init(data?: any, callback?: any) {
		return this.registerData(data, callback);
	}

	public getData(): any {
		return context.getStore();
	}

	public registerData(data?: any, callback?: any) {
		context.run(data || {}, () => {
			if (callback) {
				callback(data);
			}
		});
	}
}
export const globalContext: Context<any, any> = new AsyncHookContext();
