import { ApiProperty } from '@nestjs/swagger';

export class VehicleBasicDto {
	@ApiProperty({
		required: true,
		description: 'carPlate',
		example: '',
	})
	carPlate: string;

	@ApiProperty({
		required: true,
		description: 'mark to the vehicle',
		example: '',
	})
	mark: string;

	@ApiProperty({
		required: true,
		description: 'model to the vehicle',
		example: '',
	})
	model: number;

	@ApiProperty({
		required: true,
		description: 'colour to the vehicle',
		example: '',
	})
	colour: string;

	@ApiProperty({
		required: true,
		description: 'status to the vehicle',
		example: '',
	})
	status: string;

	@ApiProperty({
		required: true,
		description: 'assigned to the vehicle',
		example: '',
	})
	assigned: string;
}
