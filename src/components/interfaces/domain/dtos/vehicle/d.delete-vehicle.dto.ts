import { ApiProperty } from '@nestjs/swagger';

export class DeleteVehicleDto {
	@ApiProperty({
		required: true,
		description: 'message',
		example: '',
	})
	message: string;

	@ApiProperty({
		required: true,
		description: 'status',
		example: '',
	})
	status: string;
}
