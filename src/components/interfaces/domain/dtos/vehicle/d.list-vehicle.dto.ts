import { ApiProperty } from '@nestjs/swagger';
import { IsDate } from 'class-validator';

export class ListVehicleDto {
	@ApiProperty({
		required: false,
		description: 'vehicle id',
		example: '',
	})
	id: number;

	@ApiProperty({
		required: false,
		description: 'carPlate',
		example: '',
	})
	carPlate: string;

	@ApiProperty({
		required: false,
		description: 'mark to the vehicle',
		example: '',
	})
	mark: string;

	@ApiProperty({
		required: false,
		description: 'model to the vehicle',
		example: '',
	})
	model: number;

	@ApiProperty({
		required: false,
		description: 'colour to the vehicle',
		example: '',
	})
	colour: string;

	@ApiProperty({
		required: false,
		description: 'admissionDate to the vehicle',
		example: '',
	})
	@IsDate()
	admissionDate: Date;

	@ApiProperty({
		required: false,
		description: 'status to the vehicle',
		example: '',
	})
	status: string;

	@ApiProperty({
		required: false,
		description: 'assigned to the vehicle',
		example: '',
	})
	assigned: string;
}
