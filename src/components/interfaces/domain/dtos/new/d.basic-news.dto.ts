import { ApiProperty } from '@nestjs/swagger';

export class NewsDto {
	@ApiProperty({
		required: false,
		description: 'Id of the new',
		example: '',
	})
	id: number;

	@ApiProperty({
		required: true,
		description: 'Matter of the new',
		example: '',
	})
	affair: string;

	@ApiProperty({
		required: true,
		description: 'Description of the new',
		example: '',
	})
	description: string;

	@ApiProperty({
		required: true,
		description: 'Type of the new',
		example: '',
	})
	type: string;

	@ApiProperty({
		required: true,
		description: 'Id of the vehicle',
		example: '',
	})
	idVehicle: number;
}
