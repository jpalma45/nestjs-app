import { ApiProperty } from '@nestjs/swagger';
import { NewsDto } from './d.basic-news.dto';

export class UpdateNewsDto extends NewsDto {
	@ApiProperty({
		required: false,
		description: 'Id of the new',
		example: '',
	})
	id: number;
}
