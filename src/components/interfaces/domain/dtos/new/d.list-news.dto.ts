import { ApiProperty } from '@nestjs/swagger';
import { NewsDto } from './d.basic-news.dto';

export class ListNewsDto extends NewsDto {
	@ApiProperty({
		required: false,
		description: 'createdDate of the new',
		example: '',
	})
	createdDate?: Date;
}
