import {
	CreateNewsDto,
	ListNewsDto,
	UpdateNewsDto,
} from '@components/interfaces/domain/dtos';

/**
 * @summary This interface define news Service
 */
export interface NewsService {
	/**
	 * @summary This method create a new
	 * @param {CreateNewsDto} entity New information
	 */
	createNew(entity: CreateNewsDto): Promise<CreateNewsDto>;

	/**
	 * @summary This method gets a news information
	 * @param filter           						Filter information
	 */
	findNews(id: number): Promise<ListNewsDto[]>;

	/**
	 * @summary This method gets a new information
	 * @param filter           						Filter information
	 */
	findNewById(id: number): Promise<ListNewsDto>;

	/**
	 * @summary This method update a new
	 * @param {UpdateNewsDto} entity New information
	 */
	updateNew(id: number, entity: UpdateNewsDto): Promise<UpdateNewsDto>;
}
