import {
	CreateVehicleDto,
	DeleteVehicleDto,
	ListVehicleDto,
	UpdateVehicleDto,
} from '@components/interfaces/domain/dtos/vehicle';

/**
 * @summary This interface define vehicle Service
 */
export interface VehicleService {
	/**
	 * @summary This method create a new vehicle
	 * @param {CreateVehicleDto} entity vehicle information
	 */
	createVehicle(entity: CreateVehicleDto): Promise<CreateVehicleDto>;

	/**
	 * @summary This method gets a vehicle information
	 * @param filter           						Filter information
	 */
	findVehicle(id: number): Promise<ListVehicleDto>;

	/**
	 * @summary This method gets a vehicle information
	 * @param filter           						Filter information
	 */
	findVehicleByFilter(general: string): Promise<ListVehicleDto[]>;

	/**
	 * @summary This method update a vehicle
	 * @param {UpdateVehicleDto} entity vehicle information
	 */
	updateVehicle(
		id: number,
		entity: UpdateVehicleDto,
	): Promise<UpdateVehicleDto>;

	/**
	 * @summary This method delete a vehicle
	 * @param {id} vehicle id
	 */
	deleteVehicle(id: number): Promise<DeleteVehicleDto>;
}
