import {
	CreateNewsDto,
	NewsDto,
	UpdateNewsDto,
} from '@components/interfaces/domain/dtos';

/**
 * @summary This interface define News repository
 */
export interface NewsRepository {
	/**
	 * @summary This method create a new
	 * @param {CreateNewsDto} entity New information
	 */
	save(entity: CreateNewsDto): Promise<CreateNewsDto>;

	/**
	 * @summary This method gets a news information
	 * @param filter           						Filter information
	 */
	find(id: number): Promise<NewsDto[]>;

	/**
	 * @summary This method gets a new information
	 * @param filter           						Filter information
	 */
	findNewById(id: number): Promise<NewsDto>;

	/**
	 * @summary This method update a new
	 * @param {UpdateNewsDto} entity New information
	 */
	update(id: number, entity: UpdateNewsDto): Promise<UpdateNewsDto>;
}
