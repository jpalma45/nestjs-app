import {
	CreateVehicleDto,
	DeleteVehicleDto,
	ListVehicleDto,
	UpdateVehicleDto,
} from '@components/interfaces/domain/dtos/vehicle';

/**
 * @summary This interface define vehicle repository
 */
export interface VehicleRepository {
	/**
	 * @summary This method create a new vehicle
	 * @param {CreateVehicleDto} entity vehicle information
	 */
	create(entity: CreateVehicleDto): Promise<CreateVehicleDto>;

	/**
	 * @summary This method gets a vehicle information
	 * @param filter           						Filter information
	 */
	find(id: number): Promise<ListVehicleDto>;

	/**
	 * @summary This method gets a vehicle information
	 * @param filter           						Filter information
	 */
	findByFilter(general: string): Promise<ListVehicleDto[]>;

	/**
	 * @summary This method update a vehicle
	 * @param {UpdateVehicleDto} entity vehicle information
	 */
	update(id: number, entity: UpdateVehicleDto): Promise<UpdateVehicleDto>;

	/**
	 * @summary This method delete a vehicle
	 * @param {id} vehicle id
	 */
	delete(id: number): Promise<DeleteVehicleDto>;
}
