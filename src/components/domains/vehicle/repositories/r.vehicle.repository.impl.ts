import {
	CreateVehicleDto,
	DeleteVehicleDto,
	ListVehicleDto,
	UpdateVehicleDto,
} from '@components/interfaces/domain/dtos/vehicle';
import { VehicleRepository } from '@components/interfaces/repositories';
import constants from '@config/constants';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as moment from 'moment';
import { DeleteResult, Repository } from 'typeorm';
import { VehicleEntity } from '../entities/pg';

@Injectable()
export class VehicleRepositoryImpl implements VehicleRepository {
	constructor(
		@InjectRepository(VehicleEntity)
		private collection: Repository<VehicleEntity>,
	) {}

	public async create(entity: CreateVehicleDto): Promise<CreateVehicleDto> {
		const dateCreate = moment.utc(Date.now()).format('YYYY-MM-DD h:mm:ss');
		const admissionDate = new Date(dateCreate);
		const record: VehicleEntity = new VehicleEntity(
			undefined,
			entity?.carPlate,
			entity?.mark,
			entity?.model,
			entity?.colour,
			admissionDate,
			entity?.status,
			entity?.assigned,
		);
		const result = await this.collection.save(record);
		const { ...rest } = result;
		return rest;
	}

	public async find(id: number): Promise<ListVehicleDto> {
		const tables = constants().TABLES;
		const sqlEvent = `SELECT veh.id
							   , veh.carPlate
							   , veh.mark
							   , veh.model
							   , veh.colour
							   , veh.admissionDate
							   , veh.status
							   , veh.assigned
							FROM ${tables.VEHICULO} veh
							WHERE veh.id = ${id}`;

		const results = await this.collection.query(sqlEvent);
		const output: ListVehicleDto = results.map((result) => ({
			id: result.id,
			carPlate: result.carPlate,
			mark: result.mark,
			model: result.model,
			colour: result.colour,
			admissionDate: result.admissionDate,
			status: result.status,
			assigned: result.assigned,
		}));
		return output;
	}

	public async findByFilter(general: string): Promise<ListVehicleDto[]> {
		const tables = constants().TABLES;
		let sqlEvent = '';
		if (general) {
			sqlEvent = `SELECT veh.id
							   , veh.carPlate
							   , veh.mark
							   , veh.model
							   , veh.colour
							   , veh.admissionDate
							   , veh.status
							   , veh.assigned
							FROM ${tables.VEHICULO} veh WHERE
							   (veh.carPlate like '%${String(general)}%%')
							or (veh.mark like '%${String(general)}%%')
							or (veh.id = '${Number(general)}')
							or (veh.model = '${Number(general)}')
							or (veh.colour like '%${String(general)}%%')
							or (veh.status like '%${String(general)}%%')
							or (veh.assigned like '%${String(general)}%%')`;
		} else {
			sqlEvent = `SELECT veh.id
							   , veh.carPlate
							   , veh.mark
							   , veh.model
							   , veh.colour
							   , veh.admissionDate
							   , veh.status
							   , veh.assigned
							FROM ${tables.VEHICULO} veh`;
		}

		const results = await this.collection.query(sqlEvent);
		const output: ListVehicleDto[] = results.map((result) => ({
			id: result.id,
			carPlate: result.carPlate,
			mark: result.mark,
			model: result.model,
			colour: result.colour,
			admissionDate: result.admissionDate,
			status: result.status,
			assigned: result.assigned,
		}));
		return output;
	}

	public async update(
		id: number,
		entity: UpdateVehicleDto,
	): Promise<UpdateVehicleDto> {
		const VehicleUpdate = await this.collection.findOne(id);
		if (!VehicleUpdate) {
			throw new BadRequestException('vahicle not found.');
		}
		this.collection.merge(VehicleUpdate, entity);
		const update = await this.collection.save(VehicleUpdate);
		const { ...responseUpdate } = update;
		return responseUpdate;
	}

	public async delete(id: number): Promise<DeleteVehicleDto> {
		const result: DeleteResult = await this.collection.delete(id);
		if (result.affected === 0) {
			return {
				message: `The vehicle number (${id}) cannot be found`,
				status: 'FAILED',
			};
		} else {
			return {
				message: `The vehicle number (${id}) was deleted`,
				status: 'OK',
			};
		}
	}
}
