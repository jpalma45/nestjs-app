import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import {
	CreateVehicleDto,
	DeleteVehicleDto,
	ListVehicleDto,
	UpdateVehicleDto,
} from '@components/interfaces/domain/dtos/vehicle';
import { VehicleRepository } from '@components/interfaces/repositories';
import { VehicleService } from '@components/interfaces/services';
import constants from '@config/constants';
import { ArrayUtilities } from '@components/utils';

@Injectable()
export class VehicleServiceImpl implements VehicleService {
	public constructor(
		@Inject(constants().MODULES.VEHICLE.REPOSITORY.BASIC)
		private readonly vehicleRepository: VehicleRepository,
	) {}
	async findVehicleByFilter(general: string): Promise<ListVehicleDto[]> {
		const result: ListVehicleDto[] = await this.vehicleRepository.findByFilter(
			general,
		);

		if (ArrayUtilities.isEmpty(result)) {
			throw new NotFoundException(`The vehicle with (${general}) not found`);
		}
		return result;
	}

	public async createVehicle(
		entity: CreateVehicleDto,
	): Promise<CreateVehicleDto> {
		return await this.vehicleRepository.create(entity);
	}

	public async findVehicle(id: number): Promise<ListVehicleDto> {
		return await this.vehicleRepository.find(id);
	}

	public async updateVehicle(
		id: number,
		entity: UpdateVehicleDto,
	): Promise<UpdateVehicleDto> {
		const idVehicle: number = id;
		const vehicleToUpdate: UpdateVehicleDto = await this.vehicleRepository.find(
			idVehicle,
		);

		if (!vehicleToUpdate) {
			throw new NotFoundException(
				`The vehicle with id number (${id}) not found`,
			);
		}

		const updateVehicle = await this.vehicleRepository.update(id, entity);
		return updateVehicle;
	}

	public async deleteVehicle(id: number): Promise<DeleteVehicleDto> {
		return await this.vehicleRepository.delete(id);
	}
}
