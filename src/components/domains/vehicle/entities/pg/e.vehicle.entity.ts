import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';

@Entity('tbl_vehicles')
export class VehicleEntity extends BaseEntity {
	constructor(
		id: any,
		carPlate: string,
		mark: string,
		model: number,
		colour: string,
		admissionDate: Date,
		status: string,
		assigned: string,
	) {
		super();
		this.id = id;
		this.carPlate = carPlate;
		this.mark = mark;
		this.model = model;
		this.colour = colour;
		this.admissionDate = admissionDate;
		this.status = status;
		this.assigned = assigned;
	}

	@PrimaryGeneratedColumn()
	id: number;

	@Column({ length: 25, name: 'carplate' })
	carPlate: string;

	@Column({ length: 100 })
	mark: string;

	@Column()
	model: number;

	@Column({ length: 50 })
	colour: string;

	@Column({ name: 'admissiondate' })
	admissionDate: Date;

	@Column({ length: 25 })
	status: string;

	@Column({ length: 100 })
	assigned: string;
}
