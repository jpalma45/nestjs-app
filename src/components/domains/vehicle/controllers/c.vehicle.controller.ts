import {
	BadRequestException,
	Body,
	Controller,
	Delete,
	Get,
	Inject,
	Param,
	ParseIntPipe,
	Post,
	Put,
	Query,
} from '@nestjs/common';
import { ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import constants from '@config/constants';
import { VehicleServiceImpl } from '../services';
import {
	CreateVehicleDto,
	DeleteVehicleDto,
	ListVehicleDto,
	UpdateVehicleDto,
} from '@components/interfaces/domain/dtos/vehicle';

@ApiTags('Vehicles')
@Controller('/vehicles')
export class VehicleController {
	public constructor(
		@Inject(constants().MODULES.VEHICLE.SERVICE.BASIC)
		private vehicleService: VehicleServiceImpl,
	) {}

	@Get()
	@ApiQuery({
		name: 'general',
		description: 'Page number (default 1)',
		example: 1,
		required: false,
	})
	@ApiResponse({ status: 201, type: ListVehicleDto, isArray: true })
	@ApiResponse({ status: 403, description: 'Forbidden.' })
	public async getVehicles(
		@Query('general') general: string,
	): Promise<ListVehicleDto[]> {
		return await this.vehicleService.findVehicleByFilter(general);
	}

	@Get('/:id')
	@ApiResponse({ status: 201, type: ListVehicleDto })
	@ApiResponse({ status: 403, description: 'Forbidden.' })
	public async getVehiclesBtFilter(
		@Param('id', ParseIntPipe) id: number,
	): Promise<ListVehicleDto> {
		return await this.vehicleService.findVehicle(id);
	}

	@Post('/')
	@ApiResponse({ status: 201, type: CreateVehicleDto })
	@ApiResponse({ status: 403, description: 'Forbidden.' })
	public async createVehicle(
		@Body() vehicle: CreateVehicleDto,
	): Promise<CreateVehicleDto | any> {
		return await this.vehicleService.createVehicle(vehicle);
	}

	@Put('/:id')
	@ApiResponse({ status: 200, type: UpdateVehicleDto })
	@ApiResponse({ status: 403, description: 'Forbidden.' })
	public async updateVehicle(
		@Param('id', ParseIntPipe) id: number,
		@Body() body: UpdateVehicleDto,
	) {
		return await this.vehicleService.updateVehicle(id, body);
	}

	@Delete('/:id')
	@ApiResponse({ status: 200, type: DeleteVehicleDto })
	@ApiResponse({ status: 403, description: 'Forbidden.' })
	public async deleteVehicle(
		@Param('id', ParseIntPipe) id: number,
	): Promise<DeleteVehicleDto> {
		const idVehicle = Number(id);
		if (isNaN(idVehicle)) {
			throw new BadRequestException(
				`The reminder identifier should be a positive number (${idVehicle})`,
			);
		}
		return await this.vehicleService.deleteVehicle(idVehicle);
	}
}
