import { TypeOrmModule } from '@nestjs/typeorm';
import constants from '@config/constants';
import { Module } from '@nestjs/common';
import { VehicleController } from './controllers';
import { VehicleRepositoryImpl } from './repositories';
import { VehicleServiceImpl } from './services';
import { VehicleEntity } from './entities/pg';

const providers = [
	{
		provide: constants().MODULES.VEHICLE.SERVICE.BASIC,
		useClass: VehicleServiceImpl,
	},
	{
		provide: constants().MODULES.VEHICLE.REPOSITORY.BASIC,
		useClass: VehicleRepositoryImpl,
	},
];

@Module({
	imports: [TypeOrmModule.forFeature([VehicleEntity])],
	controllers: [VehicleController],
	providers: [...providers],
	exports: [...providers],
})
export class VehicleModule {}
