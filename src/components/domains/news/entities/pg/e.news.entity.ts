import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';

@Entity('tbl_news')
export class NewsEntity extends BaseEntity {
	constructor(
		id: any,
		affair: string,
		description: string,
		type: string,
		idVehicle: number,
	) {
		super();
		this.id = id;
		this.affair = affair;
		this.description = description;
		this.type = type;
		this.idVehicle = idVehicle;
	}

	@PrimaryGeneratedColumn()
	id: number;

	@Column({ length: 25, name: 'affair' })
	affair: string;

	@Column({ length: 100 })
	description: string;

	@Column()
	type: string;

	@Column({ name: 'idvehicle' })
	idVehicle: number;
}
