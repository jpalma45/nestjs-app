import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { NewsRepository } from '@components/interfaces/repositories';
import constants from '@config/constants';
import { NewsService } from '@components/interfaces/services';
import {
	CreateNewsDto,
	ListNewsDto,
	UpdateNewsDto,
} from '@components/interfaces/domain/dtos';
import { ArrayUtilities } from '@components/utils';

@Injectable()
export class NewsServiceImpl implements NewsService {
	public constructor(
		@Inject(constants().MODULES.NEWS.REPOSITORY.BASIC)
		private readonly newsRepository: NewsRepository,
	) {}
	public async createNew(entity: CreateNewsDto): Promise<CreateNewsDto> {
		return await this.newsRepository.save(entity);
	}
	async findNews(id: number): Promise<ListNewsDto[]> {
		const result: ListNewsDto[] = await this.newsRepository.find(id);

		if (ArrayUtilities.isEmpty(result)) {
			throw new NotFoundException(`The new with id (${id}) not found`);
		}
		return result;
	}
	public async findNewById(id: number): Promise<ListNewsDto> {
		const result: ListNewsDto = await this.newsRepository.findNewById(id);

		if (!result) {
			throw new NotFoundException(`The new with id (${id}) not found`);
		}
		return result;
	}
	public async updateNew(
		id: number,
		entity: UpdateNewsDto,
	): Promise<UpdateNewsDto> {
		const idNew: number = id;
		const newToUpdate: UpdateNewsDto = await this.newsRepository.findNewById(
			idNew,
		);

		if (!newToUpdate) {
			throw new NotFoundException(`The new with id number (${id}) not found`);
		}

		const updateNew = await this.newsRepository.update(id, entity);
		return updateNew;
	}
}
