import {
	CreateNewsDto,
	ListNewsDto,
	NewsDto,
	UpdateNewsDto,
} from '@components/interfaces/domain/dtos';
import { NewsRepository } from '@components/interfaces/repositories';
import constants from '@config/constants';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { NewsEntity } from '../entities/pg';

@Injectable()
export class NewsRepositoryImpl implements NewsRepository {
	constructor(
		@InjectRepository(NewsEntity)
		private collection: Repository<NewsEntity>,
	) {}
	public async save(entity: CreateNewsDto): Promise<CreateNewsDto> {
		const record: NewsEntity = new NewsEntity(
			undefined,
			entity?.affair,
			entity?.description,
			entity?.type,
			entity?.idVehicle,
		);
		const result = await this.collection.save(record);
		const { ...rest } = result;
		return rest;
	}

	public async find(id: number): Promise<ListNewsDto[]> {
		const tables = constants().TABLES;
		let sqlEvent = '';
		if (id) {
			sqlEvent = `SELECT new.id
								, new.affair
								, new.description
								, new.type
								, new.idVehicle
								, veh.carplate
								, veh.mark
								, veh.model
								, veh.status
							FROM ${tables.NEWS} new
							INNER JOIN ${tables.VEHICULO} veh ON veh.id = new.id
							WHERE new.idVehicle = ${Number(id)}`;
		} else {
			sqlEvent = `SELECT new.id
								, new.affair
								, new.description
								, new.type
								, new.idVehicle
								, veh.carplate
								, veh.mark
								, veh.model
								, veh.status
							FROM ${tables.NEWS} new
							INNER JOIN ${tables.VEHICULO} veh ON veh.id = new.id`;
		}

		const results = await this.collection.query(sqlEvent);
		const output: ListNewsDto[] = results.map((result) => ({
			idNew: result.id,
			carplate: result.carplate,
			mark: result.mark,
			model: result.model,
			status: result.status,
			carPlate: result.carPlate,
			idVehicle: result.idVehicle,
			type: result.type,
			description: result.description,
			affair: result.affair,
		}));
		return output;
	}

	public async findNewById(id: number): Promise<ListNewsDto> {
		const tables = constants().TABLES;
		const sqlEvent = `SELECT new.id
								, new.affair
								, new.description
								, new.type
								, new.idVehicle
								, veh.carplate
								, veh.mark
								, veh.model
								, veh.status
							FROM ${tables.NEWS} new
							INNER JOIN ${tables.VEHICULO} veh ON veh.id = new.id
							WHERE new.id = ${id}`;

		const results = await this.collection.query(sqlEvent);
		const output: NewsDto = results.map((result) => ({
			idNew: result.id,
			carplate: result.carplate,
			mark: result.mark,
			model: result.model,
			status: result.status,
			carPlate: result.carPlate,
			idVehicle: result.idVehicle,
			type: result.type,
			description: result.description,
			affair: result.affair,
		}));
		return output;
	}

	public async update(
		id: number,
		entity: UpdateNewsDto,
	): Promise<UpdateNewsDto> {
		const NewUpdate = await this.collection.findOne(id);
		if (!NewUpdate) {
			throw new BadRequestException('new not found.');
		}
		this.collection.merge(NewUpdate, entity);
		const update = await this.collection.save(NewUpdate);
		const { ...responseUpdate } = update;
		return responseUpdate;
	}
}
