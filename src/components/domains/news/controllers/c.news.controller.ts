import {
	Body,
	Controller,
	Get,
	Inject,
	Param,
	ParseIntPipe,
	Post,
	Put,
	Query,
} from '@nestjs/common';
import { ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import constants from '@config/constants';
import { NewsServiceImpl } from '../services';
import {
	CreateNewsDto,
	ListNewsDto,
	UpdateNewsDto,
} from '@components/interfaces/domain/dtos';

@ApiTags('News')
@Controller('/new')
export class NewsController {
	public constructor(
		@Inject(constants().MODULES.NEWS.SERVICE.BASIC)
		private newsService: NewsServiceImpl,
	) {}

	@Get()
	@ApiQuery({
		name: 'id',
		description: 'id (default 1)',
		example: 1,
		required: false,
	})
	@ApiResponse({ status: 201, type: ListNewsDto, isArray: true })
	@ApiResponse({ status: 403, description: 'Forbidden.' })
	public async getNews(@Query('id') id: number): Promise<ListNewsDto[]> {
		return await this.newsService.findNews(id);
	}

	@Get('/:id')
	@ApiResponse({ status: 201, type: ListNewsDto })
	@ApiResponse({ status: 403, description: 'Forbidden.' })
	public async getNewById(
		@Param('id', ParseIntPipe) id: number,
	): Promise<ListNewsDto> {
		return await this.newsService.findNewById(id);
	}

	@Post('/')
	@ApiResponse({ status: 201, type: CreateNewsDto })
	@ApiResponse({ status: 403, description: 'Forbidden.' })
	public async createVehicle(
		@Body() CreateNew: CreateNewsDto,
	): Promise<CreateNewsDto | any> {
		return await this.newsService.createNew(CreateNew);
	}

	@Put('/:id')
	@ApiResponse({ status: 200, type: UpdateNewsDto })
	@ApiResponse({ status: 403, description: 'Forbidden.' })
	public async updateVehicle(
		@Param('id', ParseIntPipe) id: number,
		@Body() body: UpdateNewsDto,
	) {
		return await this.newsService.updateNew(id, body);
	}
}
