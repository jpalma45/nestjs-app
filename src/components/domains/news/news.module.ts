import { TypeOrmModule } from '@nestjs/typeorm';
import constants from '@config/constants';
import { Module } from '@nestjs/common';
import { NewsController } from './controllers';
import { NewsRepositoryImpl } from './repositories';
import { NewsServiceImpl } from './services';
import { NewsEntity } from './entities/pg';
import { VehicleEntity } from '../vehicle/entities/pg';

const providers = [
	{
		provide: constants().MODULES.NEWS.SERVICE.BASIC,
		useClass: NewsServiceImpl,
	},
	{
		provide: constants().MODULES.NEWS.REPOSITORY.BASIC,
		useClass: NewsRepositoryImpl,
	},
];

@Module({
	imports: [TypeOrmModule.forFeature([VehicleEntity, NewsEntity])],
	controllers: [NewsController],
	providers: [...providers],
	exports: [...providers],
})
export class NewsModule {}
