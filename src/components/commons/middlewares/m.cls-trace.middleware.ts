import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction } from 'express';
import { globalContext } from '@components/interfaces/contexts';
import * as uuid from 'uuid';

@Injectable()
export class CLSTraceMiddleware implements NestMiddleware {
	use(req, res, next: NextFunction) {
		const traceId: string = uuid.v4();
		let store = new Map();
		store = store.set('id', traceId);
		store = store.set('startedDate', new Date());
		store = store.set('request', req);
		globalContext.init(store, () => {
			next();
		});
	}
}
