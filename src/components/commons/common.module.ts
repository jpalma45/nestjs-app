import { MiddlewareConsumer, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { typeOrmConfigAsync } from '@config/database';
import { CLSTraceMiddleware } from './middlewares';
import { NestModule } from '@nestjs/common';

@Module({
	imports: [
		ConfigModule.forRoot({ isGlobal: true, envFilePath: ['.development.env'] }),
		TypeOrmModule.forRootAsync(typeOrmConfigAsync),
	],
	providers: [],
	exports: [TypeOrmModule],
})
export class CommonModule implements NestModule {
	configure(consumer: MiddlewareConsumer) {
		consumer.apply(CLSTraceMiddleware).forRoutes('*');
	}
}
