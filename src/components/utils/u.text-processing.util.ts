import * as mom from 'moment';

export class TextProcessingUtilities {
	/**
	 * @summary This method gets a default data when value is null or undefined
	 * @param value Value to eval
	 * @param data Data when value param is null or empty
	 */
	public static nvl<T>(value: T, data: T): T {
		return !value || value === null ? data : value;
	}

	public static checkDateIsValid(date: string, formatter: string): boolean {
		return mom(date, formatter, true).isValid();
	}
}
