export class ArrayUtilities {
	/**
	 * @summary This method allow to get if the items is empty
	 * @param items List of items
	 * @returns If the array is empty
	 */
	public static isEmpty<T extends any>(items: T[] | null | undefined): boolean {
		return !items || items === null || items.length === 0;
	}
}
