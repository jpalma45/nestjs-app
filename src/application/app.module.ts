import { Module } from '@nestjs/common';
import { CommonModule } from '@components/commons/common.module';
import { NewsModule } from '@components/domains/news/news.module';
import { VehicleModule } from '@components/domains/vehicle/vehicle.module';
import { AppConfigModule } from '@config/app/config.module';

@Module({
	imports: [VehicleModule, NewsModule, CommonModule, AppConfigModule],
	controllers: [],
	providers: [],
})
export class AppModule {}
