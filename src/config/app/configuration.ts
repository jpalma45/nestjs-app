import { registerAs } from '@nestjs/config';
import { TextProcessingUtilities } from '@components/utils';

export default registerAs('app', () => ({
	env: process.env.APP_ENV,
	name: process.env.APP_NAME,
	url: process.env.APP_URL,
	port: process.env.PORT,
	admin: {
		from: TextProcessingUtilities.nvl(process.env.ADMIN_FROM, '34!'),
	},
}));
