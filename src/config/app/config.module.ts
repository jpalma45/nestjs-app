import * as Joi from '@hapi/joi';
import { Module } from '@nestjs/common';
import configuration from './configuration';
import { AppConfigService } from './config.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
/**
 * Import and provide app configuration related classes.
 *
 * @module
 */
@Module({
	imports: [
		ConfigModule.forRoot({
			load: [configuration],
			validationSchema: Joi.object({
				APP_NAME: Joi.string().default('PulpoApp'),
				APP_ENV: Joi.string()
					.valid('local', 'development', 'production', 'test')
					.default('local'),
				APP_URL: Joi.string().default('http://localhost'),
				APP_PORT: Joi.number().default(8080),
			}),
		}),
	],
	providers: [ConfigService, AppConfigService],
	exports: [ConfigService, AppConfigService],
})
export class AppConfigModule {}
