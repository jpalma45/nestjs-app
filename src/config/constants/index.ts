export default () => ({
	MODULES: {
		NEWS: {
			SERVICE: {
				BASIC: 'NewsService',
			},
			REPOSITORY: {
				BASIC: 'MongoNews',
			},
		},
		VEHICLE: {
			SERVICE: {
				BASIC: 'VehicleService',
			},
			REPOSITORY: {
				BASIC: 'postgresVehicle',
			},
		},
	},
	TABLES: {
		VEHICULO: 'tbl_vehicles',
		NEWS: 'tbl_news',
	},
});
