import { ConfigModule, ConfigService } from '@nestjs/config';
import {
	TypeOrmModuleOptions,
	TypeOrmModuleAsyncOptions,
} from '@nestjs/typeorm';
import * as path from 'path';

export default class TypeOrmConfig {
	static getormConfig(config: ConfigService): TypeOrmModuleOptions {
		const route = path.resolve(__dirname, '../../');
		return {
			type: 'postgres',
			host: config.get('DATABASE_HOST'),
			port: parseInt(config.get('DATABASE_PORT')),
			username: config.get('DATABASE_USERNAME'),
			password: config.get('DATABASE_PASSWORD'),
			database: config.get('DATABASE_NAME'),
			synchronize: false,
			autoLoadEntities: false,
			logging: false,
			entities: [`${route}/**/*.entity.{ts,js}`],
			subscribers: [`${route}/**/*.esubscriber.{ts,js}`],
			extra: {
				connectionLimit: parseInt(config.get('DATABASE_CONNECTION_LIMIT')),
				charset: 'utf8_general_ci',
			},
		};
	}
}

export const typeOrmConfigAsync: TypeOrmModuleAsyncOptions = {
	imports: [ConfigModule],
	useFactory: async (config: ConfigService): Promise<TypeOrmModuleOptions> =>
		TypeOrmConfig.getormConfig(config),
	inject: [ConfigService],
};
