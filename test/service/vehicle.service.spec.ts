import { VehicleService } from '@components/interfaces/services';
import { VehicleServiceImpl } from '@components/domains/vehicle/services';
import {
	CreateVehicleDto,
	UpdateVehicleDto,
} from '@components/interfaces/domain/dtos/vehicle';
import constants from '@config/constants';
import { Test, TestingModule } from '@nestjs/testing';

describe('VehicleServiceImpl', () => {
	const VehicleMock: CreateVehicleDto = {
		mark: 'TOYOTA',
		model: 2019,
		colour: 'NEGRO',
		status: 'ACTIVO',
		assigned: 'JPALMA',
		carPlate: 'XDR87',
	};
	let vehicleService: VehicleService;
	const activityRepository = {
		find: jest.fn().mockImplementation((id: number) => {
			if (id === 1) {
				return Promise.resolve(VehicleMock);
			} else {
				return Promise.resolve([]);
			}
		}),

		findByFilter: jest.fn().mockImplementation((filter: string) => {
			if (filter === 'NEGRO') {
				return Promise.resolve([VehicleMock]);
			} else {
				return Promise.resolve([]);
			}
		}),

		create: jest.fn().mockImplementation((entity) => {
			Object.assign(entity, { id: 2 });
			return entity;
		}),

		update: jest.fn().mockImplementation((id: number, dataUpdate) => {
			if (id === 1) {
				return Promise.resolve({
					colour: 'NEGRO',
					carPlate: 'ASD13',
					mark: 'MAZDA',
					model: 2016,
					status: 'ACTIVE',
					assigned: 'Jpalma',
				});
			} else {
				return Promise.resolve([]);
			}
		}),

		delete: jest.fn().mockImplementation((id: number) => {
			if (id === 1) {
				return Promise.resolve({
					message: `The vehicle number (${id}) was deleted`,
					status: 'OK',
				});
			} else {
				return Promise.resolve([]);
			}
		}),
	};

	beforeAll(async () => {
		const moduleRef: TestingModule = await Test.createTestingModule({
			providers: [
				VehicleServiceImpl,
				{
					provide: constants().MODULES.VEHICLE.REPOSITORY.BASIC,
					useValue: activityRepository,
				},
			],
		}).compile();
		vehicleService = moduleRef.get(VehicleServiceImpl);
	});

	describe('createVehicle method', () => {
		it('success register vehicle', async () => {
			const result = await vehicleService.createVehicle(VehicleMock);
			expect(result.assigned).toBe('JPALMA');
			expect(result.status).toBe('ACTIVO');
		});
	});

	describe('find method', () => {
		it('success find vehicle', async () => {
			const result = await vehicleService.findVehicle(1);
			expect(result.assigned).toBe('JPALMA');
			expect(result.status).toBe('ACTIVO');
		});
	});

	describe('find method', () => {
		it('success findVehicleByFilter vehicle', async () => {
			const result = await vehicleService.findVehicleByFilter('NEGRO');
			expect(result[0].assigned).toBe('JPALMA');
			expect(result[0].status).toBe('ACTIVO');
		});
	});

	describe('update method', () => {
		it('success update vehicle', async () => {
			const dataUpdate: UpdateVehicleDto = {
				colour: 'NEGRO',
				carPlate: 'ASD13',
				mark: 'MAZDA',
				model: 2016,
				status: 'INACTIVE',
				assigned: 'Jpalma',
			};

			const result = await vehicleService.updateVehicle(1, dataUpdate);
			expect(result.assigned).toBe('Jpalma');
			expect(result.status).toBe('ACTIVE');
		});
	});

	describe('DELETE method', () => {
		it('success delete vehicle', async () => {
			const result = await vehicleService.deleteVehicle(1);
			expect(result.message).toBe('The vehicle number (1) was deleted');
			expect(result.status).toBe('OK');
		});
	});
});
